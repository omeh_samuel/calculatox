/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
} from 'react-native';

let counter = [];
class App extends Component {
  state = {
    count: 0,
    resultT: '',
    calculated:''
  };
  operators = ['Del', 'C', '+', '-', '*', '/'];

  calculateText() {
    let text = this.state.resultT;
    ///prsing is done here
    this.setState({
      calculated: eval(text),
    });

  }
  validate(){
    const text = this.state.resultT;
    switch(text.slice(-1)){
      case '+':
      case '-':
      case '*':
      case '/':  
          return false;    
    }
    return true;
  }

  checker(element) {
    return counter.indexOf(element) !== -1;
  }
  buttonPressed(i) {
    if (i === '=') {
      return this.validate()&&this.calculateText();
    }
    let r = this.state.resultT;
    let re = r + i;
    this.setState({resultT: re});
  }
  operate(operation) {
    switch (operation) {
      case 'Del':
        let text = this.state.resultT.split('');
        text.pop();
        this.setState({
          resultT: text.join(''),
        });
        break;
      case 'C':
        let text = this.state.resultT.split('');
        for(let i = 0;i<text.length;i++){
          text.pop();
        }
        this.setState({
          resultT:text.join(''),
        });

        break;
      case '+':
      case '-':
      case '*':
      case '/':
        const op = this.state.resultT.split('').pop();
        if (this.operators.indexOf(op)>0) return;

        if (this.state.resultT == '') return;
        this.setState({
          resultT: this.state.resultT + operation,
        });
    }
  }

  render() {
    let rows = [];
    let column = [];

    let nums = [[7, 8, 9], [4, 5, 6], [1, 2, 3], ['.', 0, '=']];
    for (let p = 0; p < 5; p++) {
      column.push(
        <TouchableOpacity
          style={styles.operationBtn}
          onPress={() => this.operate(this.operators[p])}
          key={this.operators[p]+1}
          >
          <Text style={styles.columnText}>{ this.operators[p]}</Text>
        </TouchableOpacity>,
      );
    }
    for (let i = 0; i < 4; i++) {
      let row = [];
      for (let j = 0; j < 3; j++) {
        row.push(
          <TouchableOpacity
            onPress={() => this.buttonPressed(nums[i][j])}
            style={styles.resultBtn}
            key={nums[i][j]}
            >
            <Text style={styles.text}>{nums[i][j]}</Text>
          </TouchableOpacity>,
        );
      }
      rows.push(<View key={i} style={styles.row}>{row}</View>);
    }
    return (
      <View style={styles.container}>
        <View style={styles.calculation}>
          <Text style={styles.CalculationText}>{this.state.resultT}</Text>
        </View>
        <View style={styles.result}>
          <Text style={styles.resultText}>{this.state.calculated}</Text>
        </View>
        <View style={styles.buttons}>
          <View style={styles.numbers}>{rows}</View>
          <View style={styles.operations}>
            <View style={styles.column}>{column}</View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  text: {
    fontSize: 30,
    color:"#fff"
  },
  container: {
    flex: 1,
  },
  calculation: {
    backgroundColor: '#f2f2f2',
    flex: 1.5,
    padding: 10,
    justifyContent: 'flex-end',
    flexDirection: 'row',
    alignItems: 'center',
  },
  result: {
    flex: 1,
    backgroundColor: '#f7f7f7',
    justifyContent: 'flex-end',
    flexDirection: 'row',
    padding: 15,
    alignItems: 'center',
  },
  buttons: {
    // backgroundColor: '#777',
    flexDirection: 'row',
    flexGrow: 8,
  },
  operations: {
    backgroundColor: '#0edcff',
    flex: 1,
  },
  operationBtn: {
    padding: 10,
    backgroundColor: '#0effeb',
    flex: 1,
    alignItems: 'center',
    alignSelf: 'stretch',
    justifyContent: 'center',
  },
  numbers: {
    flex: 3,
    backgroundColor: '#242424',
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    flex: 1,
  },
  column: {
    flex: 1,
    justifyContent: 'space-between',
  },
  columnText: {
    fontSize: 35,
    color: '#6e43cb',
  },
  resultText: {
    fontSize: 18,
  },
  CalculationText: {
    fontSize: 25,
  },
  resultBtn: {
    padding: 10,
    backgroundColor: 'lightslategrey',
    flex: 1,
    alignItems: 'center',
    alignSelf: 'stretch',
    justifyContent: 'center',
  },
});

export default App;
